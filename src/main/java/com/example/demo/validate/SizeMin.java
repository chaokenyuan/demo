package com.example.demo.validate;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy= {SizeMinValidator.class})
public @interface SizeMin {

	int min();
	boolean allowEmpty();
	String message="欄位長度檢核錯誤";
	Class<?>[] groups() default{};
	Class<? extends Payload>[] payload() default {};
}
