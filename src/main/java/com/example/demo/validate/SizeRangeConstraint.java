package com.example.demo.validate;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {SizeRangeValidator.class})
public @interface SizeRangeConstraint {
	
	int min();
	int max();
	boolean isEmpty();
	String message() default "欄位長度檢核錯誤";
	
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};

}
