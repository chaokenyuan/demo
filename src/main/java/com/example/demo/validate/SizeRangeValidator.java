package com.example.demo.validate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

public class SizeRangeValidator implements ConstraintValidator<SizeRangeConstraint, String> {

	private int min;
	private int max;
	private boolean isEmpty;

	@Override
	public void initialize(SizeRangeConstraint constraintAnnotation) {
		this.min = constraintAnnotation.max();
		this.max = constraintAnnotation.max();
		this.isEmpty = constraintAnnotation.isEmpty();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {

		if (!isEmpty && StringUtils.isBlank(value)) {
			return true;
		}
		int len = StringUtils.defaultIfBlank(value, "").length();
		if (len >= min && len <= max) {
			return true;
		} else {
			return false;
		}
	}

}
