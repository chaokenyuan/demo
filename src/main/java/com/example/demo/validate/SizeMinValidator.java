package com.example.demo.validate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

public class SizeMinValidator implements ConstraintValidator<SizeMin, String> {

	private int min;
	private boolean allowEmpty;

	@Override
	public void initialize(SizeMin constrainAnnotation) {

		this.min = constrainAnnotation.min();
		this.allowEmpty = constrainAnnotation.allowEmpty();

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {

		if (allowEmpty && StringUtils.isBlank(value)) {
			return true;
		}

		int len = StringUtils.defaultIfBlank(value, "").length();

		if (len >= min) {
			return true;
		} else {
			return false;
		}

	}

}
