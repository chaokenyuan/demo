package com.example.demo.exception;

import com.example.demo.enums.DemoEnums;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DemoException extends RuntimeException {

	// 在自訂的Exception 中，可指定 自定義的 enums 回報錯誤訊息

	protected DemoEnums returnCode;

	protected String returnMessage;

	public DemoException(String returnMessage) {
		super(returnMessage);
		this.returnMessage = returnMessage;
	}

	public DemoException(DemoEnums returnCode) {
		this.returnCode = returnCode;
	}

	public DemoException(DemoEnums returnCode, String returnMessage) {
		super(returnMessage);
		this.returnCode = returnCode;
		this.returnMessage = returnMessage;
	}

}
