package com.example.demo.controller.advice;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.RequestContext;

@Aspect
@Component
public class LogResponseAspect {


    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private HttpServletRequest request; // 注入 http protocol 以抓取 request 資訊
    
    @Autowired
    private WebRequestContext requestContext; // 注入自定資訊

    
    @Around(value = "execution(public * com.example.demo.controller.*Controller.**(..))")
    public Object around (ProceedingJoinPoint joinPoint) throws Throwable {
    	
    	Object[] args = joinPoint.getArgs();
    	
    	String seq = request.getHeader("");
    	String url = request.getRequestURI().replaceAll(request.getContextPath(), "");
    	
    	requestContext.setApi(url);
    	requestContext.setSeq(seq);
    	
    	return joinPoint.proceed();
    	
    }
    
//    @Pointcut("execution(public * com.example.demo.*.controller.*Controller.*(..))")
//    public void countrollerPointcut() {
//
//    }
//
//    @Pointcut("execution(public * com.example.demo.*.controller.advice.WebErrorHandler.*(..))")
//    public void advicePointcut() {
//
//    }


    
}
