package com.example.demo.controller.advice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.example.demo.dto.base.DemoResponse;
import com.example.demo.dto.base.ErrorInfo;
import com.example.demo.enums.DemoEnums;
import com.example.demo.exception.DemoException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@ControllerAdvice
public class WebErrorHandler {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Value(value = "demo.id")
	protected String demoId;

	@Autowired
	private WebRequestContext context;

	@Autowired
	private ObjectMapper objectMapper;

	@ExceptionHandler(DemoException.class)
	public ResponseEntity<?> onDemoException(DemoException e) {

		logger.error("Log {},{}, DemoException -> {}", demoId, context.getSeq(), e.getMessage(), e);

		DemoResponse<JsonNode> response = new DemoResponse<JsonNode>();

		String reasonCode = null;
		String reasonDesc = null;

		if (e.getReturnCode() != null) {

			reasonCode = e.getReturnCode().getReturnCode();
			reasonDesc = e.getReturnCode().getReturnMessage();

		} else {
			String returnDesc = String.format(DemoEnums.FAILED.getReturnMessage(), e.getMessage());

			reasonCode = DemoEnums.FAILED.getReturnCode();
			reasonDesc = returnDesc;
		}

		HttpStatus httpStatus = HttpStatus.BAD_REQUEST;

		response.setData(objectMapper.createObjectNode());

		response.setErrorInfo(genErrorInfo(httpStatus, reasonCode, reasonDesc));

		logger.info("Log {} {} DemoException response -> {}", demoId, context.getSeq(), response);

		return new ResponseEntity<DemoResponse<JsonNode>>(response, httpStatus);

	}

	private ErrorInfo genErrorInfo(HttpStatus httpStatus) {

		return genErrorInfo(httpStatus, null, null);

	}

	private ErrorInfo genErrorInfo(HttpStatus httpStatus, String reasonCode, String reasonDesc) {

		return ErrorInfo.builder().code(httpStatus.value()).message(httpStatus.getReasonPhrase())
				.reasonCode(reasonCode).build();

	}

}
