package com.example.demo.controller.advice;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import lombok.Data;

/**
 * 做為檔頭或是其他特殊資訊
 * @author chaokenyuan
 *
 */

@Data
@Component
@RequestScope
public class WebRequestContext {

	public static final String SEQ_ID="SEQ_ID";
	
	public static final String APP_ID="APP_ID";
	
	private String api;
	private String seq;
	
}
