package com.example.demo.controller.base;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.example.demo.exception.DemoException;

@CrossOrigin(origins = "*", allowedHeaders = "*", allowCredentials = "true")

public abstract class BaseController {

	public void requestVaild(Errors errors) throws DemoException {

		StringBuilder desc = new StringBuilder();

		if (errors.hasErrors()) {

			for (ObjectError error : errors.getAllErrors()) {

				desc.append(", ").append(error.getDefaultMessage());

			}
			String message = desc.toString().length() > 2 ? desc.toString().substring(2) : "Request Vaild Error";
			throw new DemoException(message);

		}
	}

}
