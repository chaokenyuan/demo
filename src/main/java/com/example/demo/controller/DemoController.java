package com.example.demo.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.controller.base.BaseController;
import com.example.demo.dto.QueryMsgListTranRq;
import com.example.demo.dto.QueryMsgListTranRs;
import com.example.demo.dto.base.DemoResponse;
import com.example.demo.service.DemoService;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@RestController
public class DemoController extends BaseController {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private DemoService demoService;
	
	
	@ApiOperation(value = "API")
	@RequestMapping(value="/demo", method = RequestMethod.POST)
	public @ResponseBody DemoResponse<QueryMsgListTranRs> queryMsgList(@Valid @RequestBody QueryMsgListTranRq request, @ApiIgnore Errors errors) throws Exception{
		
		logger.debug("queryMsg#start");
		requestVaild(errors);
		demoService.checkRequestVal(request);
		return demoService.queryMsgList(request);
		
	}
}
