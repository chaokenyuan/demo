package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.controller.base.BaseController;
import com.example.demo.dto.QueryUserRequest;
import com.example.demo.dto.QueryUserResponse;
import com.example.demo.service.impl.QueryUserServiceImpl;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@RestController
public class QueryUserController extends BaseController {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private QueryUserServiceImpl queryUserService;
	
	
	@ApiOperation(value = "API")
	@RequestMapping(value="/query", method = RequestMethod.POST)
	public @ResponseBody QueryUserResponse queryUser( @RequestBody QueryUserRequest request, @ApiIgnore Errors errors) throws Exception{
		

		return queryUserService.getQueryUserResponse(request);
		
	}
}
