package com.example.demo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class QueryMsgListTranRq {
	
	
	@ApiModelProperty(value="ID")
	private String id;
	

}
