package com.example.demo.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class QueryUserResponse {

	@JsonProperty("USER_LIST")
	private List<User> userList= new ArrayList<User>();

	@Data
	public static class User {

		@JsonProperty("ID")
		private String id;

		@JsonProperty("NAME")
		private String name;

	}

}
