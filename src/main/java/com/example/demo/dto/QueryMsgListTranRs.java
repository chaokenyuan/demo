package com.example.demo.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class QueryMsgListTranRs {

	
	@JsonProperty("TOTAL")
	private String total;
	
	@JsonProperty("ROWS")
	private List<Rows> rows = new ArrayList<>();
	
	@Data
	public static class Rows{
		
		@JsonProperty("SEQ")
		private String seq;
		
		@JsonProperty("ID")
		private String id;
		
	}
}
