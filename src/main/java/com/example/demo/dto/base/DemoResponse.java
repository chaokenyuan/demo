package com.example.demo.dto.base;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DemoResponse<T> {
	
	
	@JsonProperty("DATA")
	private T data;
	
	@JsonProperty("ERROR")
	private ErrorInfo errorInfo = ErrorInfo.builder().build();
	
	public DemoResponse(T data) {
		this.data  = data;
	}
	

	public DemoResponse(ErrorInfo errorInfo) {
		this.errorInfo = errorInfo;
	}
}
