package com.example.demo.dto.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(Include.NON_NULL)
public class ErrorInfo {
	

	@JsonProperty("CODE")
	private Integer code;
	
	@JsonProperty("MESSAGE")
	private String message;
	
	@JsonProperty("REASON_CODE")
	private String reasonCode;
	
	@JsonProperty("RESON_DESC")
	private String reasonDesc;
	

}
