package com.example.demo.interceptor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

public class RestTemplateLoggingInterceptor implements ClientHttpRequestInterceptor {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {

		traceRequest(request, body);

		ClientHttpResponse response = null;

		try {

			response = execution.execute(request, body);

		} catch (Throwable t) {
			throw t;
		} finally {

			traceResponse(response);

		}

		return response;

	}

	private String getBodyAsJson(String bodyString) {

		if (bodyString == null || bodyString.length() == 0) {
			return null;
		} else {
			if (isValidJSON(bodyString)) {
				return bodyString;
			} else {
				bodyString = bodyString.replaceAll("\"", "\\\"");
				return "\"" + bodyString + "\"";
			}
		}
	}

	private String getRequestBody(byte[] body) throws UnsupportedEncodingException {
		if (body != null && body.length > 0) {
			return getBodyAsJson(new String(body, "UTF-8"));
		} else {
			return null;
		}
	}

	private void traceRequest(HttpRequest request, byte[] body) throws IOException {
		logger.info("request URI :" + (request != null ? request.getURI() : "null"));
		logger.info("request method :" + (request != null ? request.getMethod() : "null"));
		logger.info("request body :" + getRequestBody(body));
	}

	private void traceResponse(ClientHttpResponse response) throws IOException {

		String body = getBodyString(response);
		logger.info("response status code :" + (response != null ? response.getStatusCode() : "null"));
		logger.info("response status text :" + (response != null ? response.getStatusText() : "null"));
		logger.info("response body :" + body);
	}

	private String getBodyString(ClientHttpResponse response) {

		try {

			if (response != null && response.getBody() != null && isReadableResponse(response)) {

				StringBuilder inputStringBuilder = new StringBuilder();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getBody(), "UTF-8"));

				String line = bufferedReader.readLine();

				while (line != null) {
					inputStringBuilder.append(line);
					inputStringBuilder.append('\n');
					line = bufferedReader.readLine();
				}
				return inputStringBuilder.toString();
			} else {
				return null;
			}

		} catch (IOException e) {

			logger.error(e.getMessage(), e);
			return null;
		}
	}

	private boolean isReadableResponse(ClientHttpResponse response) {

		if (response.getHeaders().get("Content-Type") != null) {
			for (String contentType : response.getHeaders().get("Content-Type")) {
				if (isReadableContentType(contentType)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isReadableContentType(String contentType) {
		return contentType.startsWith("application/json") || contentType.startsWith("text");
	}

	public boolean isValidJSON(final String json) {
		boolean valid = false;
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.readTree(json);

		} catch (IOException e) {
			logger.error("Read value from json string failed \nJson:{}", json, e);
			valid = false;
		}
		return valid;

	}
}
