package com.example.demo.enums;

/**
 * 
 * 自定義 enums 以方便訊息格式化，必須要宣告為 public
 * 且只有 getter 
 * @author chaokenyuan
 *
 */
public enum DemoEnums {

	SUCCESS("0000", "Success"), 
	FAILED("E001", "Failed");

	private String returnCode;
	private String returnMessage;

	DemoEnums(String returnCode, String returnMessage) {
		this.returnCode = returnCode;
		this.returnMessage = returnMessage;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

}
