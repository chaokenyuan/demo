package com.example.demo.config;

import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.example.demo.interceptor.RestTemplateLoggingInterceptor;

@Configuration
public class RestTemplateConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(RestTemplateConfig.class);

	@Value("${restTemplate.connect-timeout.millisecond}")
	String connectTimeoutMillisecond;

	@Bean
	@Primary
	public RestTemplate getRestTemplate() {

		final HttpClientBuilder clientBuilder = HttpClientBuilder.create();
		clientBuilder.useSystemProperties();

		return null;

	}

	public RestTemplate genSSLRestTemplate(HttpClientBuilder clientBuilder) {

		RestTemplate restTemplate = null;

		TrustStrategy acceptingTrustStrategy = (X509Certificate[] certificate, String authType) -> true;

		SSLContext sslContext = null;

		try {
			sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
		} catch (Exception ex) {
			sslContext = null;

			LOGGER.error("RestTemplate Exception", ex);

		}

		if (sslContext != null) {
			SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, new TrustAllHostnameVerifier());

			final CloseableHttpClient client = clientBuilder.setSSLSocketFactory(csf).build();

			PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
			connectionManager.setDefaultMaxPerRoute(1000);
			connectionManager.setMaxTotal(1000);

			final HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(
					HttpClientBuilder.create().setConnectionManager(connectionManager).build());
			factory.setHttpClient(client);

			factory.setConnectTimeout(Integer.valueOf(connectTimeoutMillisecond));
			factory.setReadTimeout(Integer.valueOf(connectTimeoutMillisecond));

			restTemplate = new RestTemplate(new BufferingClientHttpRequestFactory(factory));

			restTemplate.getInterceptors().add(new RestTemplateLoggingInterceptor());

		}
		return restTemplate;

	}
	
	public class TrustAllHostnameVerifier implements HostnameVerifier{
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	}

}
