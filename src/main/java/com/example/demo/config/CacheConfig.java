package com.example.demo.config;

import java.util.concurrent.TimeUnit;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

import com.google.common.cache.CacheBuilder;

@Configuration
@EnableCaching
public class CacheConfig extends CachingConfigurerSupport {

	private static final Integer CACHE_EXPIRED_MINUTES = 5;

	@Bean
	@Primary
	@Override
	@Scope
	public CacheManager cacheManager() {

		return new ConcurrentMapCacheManager();

	}

	@Bean
	public CacheManager fiveMinitesCacheManager() {

		ConcurrentMapCacheManager cacheManager = new ConcurrentMapCacheManager() {
			@Override
			protected Cache createConcurrentMapCache(final String name) {
				return new ConcurrentMapCache(name, CacheBuilder.newBuilder()
						.expireAfterWrite(CACHE_EXPIRED_MINUTES, TimeUnit.MINUTES).build().asMap(), false);
			}
		};
		return cacheManager;
	}

}
