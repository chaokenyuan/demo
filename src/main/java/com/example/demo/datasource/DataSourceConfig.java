package com.example.demo.datasource;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

//@Configuration
public class DataSourceConfig {

	@Value("${app.datasource.url}")
	private String dataSourceUrl;

	@Profile({ "local", "sit", "uat", "prod" })
	@Bean(name = "dataSource")
	public DataSource getProdDataSource() throws Exception {

		return null;
	}

}
