package com.example.demo.service;

import com.example.demo.dto.QueryMsgListTranRq;
import com.example.demo.dto.QueryMsgListTranRs;
import com.example.demo.dto.base.DemoResponse;


public interface DemoService {
	
	void checkRequestVal(QueryMsgListTranRq request) throws Exception;
	
	DemoResponse<QueryMsgListTranRs> queryMsgList(QueryMsgListTranRq request) throws Exception;

}
