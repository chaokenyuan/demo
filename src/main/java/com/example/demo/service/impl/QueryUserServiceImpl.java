package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.QueryUserRequest;
import com.example.demo.dto.QueryUserResponse;
import com.example.demo.entity.UserEntity;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.QueryUserService;

@Service
public class QueryUserServiceImpl implements QueryUserService {

	@Autowired
	private UserRepository userRepository;


	@Override
	public QueryUserResponse getQueryUserResponse(QueryUserRequest request) {

		List<UserEntity> userList = (List<UserEntity>) userRepository.findAll();

		ArrayList<QueryUserResponse.User> userDtoList = new ArrayList<QueryUserResponse.User>();

		for (UserEntity userEntity : userList) {

			QueryUserResponse.User userDto = new QueryUserResponse.User();
			userDto.setId(userEntity.getId());
			userDto.setName(userEntity.getName());

			userDtoList.add(userDto);

		}

		QueryUserResponse response = new QueryUserResponse();
		response.setUserList(userDtoList);

		return response;
	}

}
