package com.example.demo.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.example.demo.dto.QueryMsgListTranRq;
import com.example.demo.dto.QueryMsgListTranRs;
import com.example.demo.dto.base.DemoResponse;
import com.example.demo.enums.DemoEnums;
import com.example.demo.exception.DemoException;
import com.example.demo.service.DemoService;

@Service
public class DemoServiceImpl implements DemoService {

	private Logger logger = LoggerFactory.getLogger(getClass());

	private final static DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	private final String PERSORN_EGEX = "^[a-zA-Z0-9]{10}$";

	private final String PHONE_REGEX = "^09[0-9]{8}$";

	private final String UID_REGEX = "^U[a-f0-9]{32}$";

	private final String EMAIL_REGEX = "^[A-Za-z0-9_]{1,63}@[A-Za-z0-9]{2,63}\\.[A-Za-z0-9]{2,63}(\\.[A-Za-z0-9]{2,63})?$";

	private final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	public void checkRequestVal(QueryMsgListTranRq request) throws Exception {

		if (StringUtils.isNotBlank(request.getId()) && !request.getId().matches(UID_REGEX)) {

			throw new DemoException("");

		}

	}

	@Override
	public DemoResponse<QueryMsgListTranRs> queryMsgList(QueryMsgListTranRq request) throws Exception {
		DemoResponse<QueryMsgListTranRs> response = new DemoResponse();

		QueryMsgListTranRs tranRs = new QueryMsgListTranRs();

		List<QueryMsgListTranRs.Rows> rowList = new ArrayList<>();

		QueryMsgListTranRs.Rows rows = null;
		
		try {
			/**
			 * Logic
			 */
		}catch (Exception e) {
			logger.error("");
			throw new DemoException(DemoEnums.FAILED);
		}

		tranRs.setTotal("");
		response.setData(tranRs);
		
		return response;

	}

}
