package com.example.demo.service;

import com.example.demo.dto.QueryUserRequest;
import com.example.demo.dto.QueryUserResponse;

public interface QueryUserService {
	
	public QueryUserResponse getQueryUserResponse(QueryUserRequest request);

}
