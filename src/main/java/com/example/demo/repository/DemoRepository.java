package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Demo;

@Repository
public interface DemoRepository extends CrudRepository<Demo,Long>{
	
	@Query("select t from Demo t where t.demoNo = :demoNo ")
	List<Demo> findDemoList(@Param("demoNo") String demoNo);

}
